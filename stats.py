class Module:
    def __init__(self):
        self.grades = dict()

    def add_student(self, lastname, firstname, grades):
        self.grades[(lastname, firstname)] = grades

    def n_students(self):
        return len(self.grades)
    
    def load_from_file(self, file_content):
        lines = file_content.split("\n")
        for line in lines:
            tmp = line.split(", ")
            self.add_student(tmp[0], tmp[1], [int(mark) for mark in tmp[2:]])

    def average_grade(self, lastname, firstname):
        g = self.grades[(lastname, firstname)]
        total = float(sum(g))
        n_grades = len(g)
        return total / n_grades
