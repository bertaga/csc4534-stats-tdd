from pytest import approx, fixture
from stats import Module

@fixture
def module():
    m = Module()
    m.add_student("Durden", "Tyler", [15, 10, 12])
    m.add_student("Singer", "Marla", [12, 14, 5])
    m.add_student("Paulson", "Bob", [5, 6, 10])
    m.add_student("Face", "Angel", [12, 20, 14])
    return m

@fixture
def file_content():
    return """Durden, Tyler, 15, 10, 12
Singer, Marla, 12, 14, 5
Paulson, Bob, 5, 6, 10
Face, Angel, 12, 20, 14"""

def test_nb_eleves(module):
    assert module.n_students() == 4
    
def test_average_grade(module):
    assert module.average_grade("Durden", "Tyler") == approx(12.33, 0.01)
    assert module.average_grade("Singer", "Marla") == approx(10.33, 0.01)
    assert module.average_grade("Paulson", "Bob") == approx(7.00, 0.01)
    assert module.average_grade("Face", "Angel") == approx(15.33, 0.01)

def test_load_from_file(file_content):
    m = Module()
    m.load_from_file(file_content)
    assert m.n_students() == 4
    assert m.grades[("Durden", "Tyler")] == [15, 10, 12]
    assert m.grades[("Singer", "Marla")] == [12, 14, 5]
    assert m.grades[("Paulson", "Bob")] == [5, 6, 10]
    assert m.grades[("Face", "Angel")] == [12, 20, 14]
